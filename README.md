# Container Image CI Templates

This project has the objective to supply an easy to use gitlab CI template to build
images, helm charts and possibly other OCI artifacts.
The focus of the templates made available here are that the user can simply import
the respective builder and by setting specific flags/variables the pipeline will act
acordingly to the user requirements.
This allows the user to use a variety of features with a simple true/false flag
(or similar) without requiring to create complex pipelines.
We try to generalize the available features the most we can, so some features might
not be available or not adequate for what you want. In this case you should develop
your own personalized pipeline.

In any case, the project does not aim to replace a fully and well setup gitlab CI/CD
pipeline, but instead the steps to build and execute some operations on OCI artifacts

We also aim to be compatible with other repositories that are OCI compliant, so the
scripts should work for upstream projects as well as Harbor. At this time we test:
* Harbor registry @ registry.cern.ch
* Gitlab registry @ gitlab-registry.cern.ch

For a full list of features that are available, check the table below:

## Generic Flags

* REGISTRY_USER - The username to use to connect to the remote registry
* REGISTRY_PASSWORD - The password/token to use to connect to the remote registry
* REGISTRY_IMAGE_PATH / REGISTRY_CHART_PATH - Defines where the artifact should be pushed to.
* CONTEXT_DIR (containers only) - Defines the context dir for the image build
* DOCKER_FILE_NAME (containers only) - Defines the name of the Dockerfile

## Options

|  feature/template  | kaniko | docker | helm | default value |
|:------------------:|:------:|:------:|:----:|:-------------:|
|  ACCELERATED_IMAGE |    ✅   |    ✅   |   ❌  |    "false"    |
|     BUILD_ARGS     |    ✅   |    ✅   |   ❌  |       ""      |
|   CACHE_{FROM/TO}  |    ❌   |    ✅   |   ❌  |       ""      |
| COSIGN_PRIVATE_KEY |    ✅   |    ✅   |   ✅  |       ""      |
|      PLATFORMS     |    ❌   |    ✅   |   ❌  |       ""      |
| PUSH_{IMAGE/CHART} |    ✅   |    ✅   |   ✅  |    "false"    |
|     EXTRA_TAGS     |    ✅   |    ✅   |   ❌  |       ""      |
