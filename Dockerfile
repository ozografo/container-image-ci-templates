# We download from gitlab registry on purpose. See: https://gitlab.cern.ch/ci-tools/container-image-ci-templates/-/issues/9
FROM gitlab-registry.cern.ch/ci-tools/container-image-ci-templates/alpine:latest

# adding a file in order to verify the image build and push
RUN echo "verifying-default-dockerfile" > tmp/test
